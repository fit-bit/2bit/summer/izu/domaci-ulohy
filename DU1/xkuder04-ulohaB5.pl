ulohaB5(L, LGT, LLT) :-
   two_list_duplicates(LLT, LGT),
   append(LGT, LLT, Merge),
   all_values_used(L, Merge),
   check_list_counts(L, LGT, LLT),
   check_list_greater(LGT, L),
   check_list_less(LLT, L).

% Kontrola jestli v LLT a LGT nejsou nějaké stejné prvky
two_list_duplicates([], _X).
two_list_duplicates([H|Y], X) :-
   not(member(H, X)),
   two_list_duplicates(Y, X).

% Kontrola jestli všechny prvky z L byly využity
all_values_used([], _X).
all_values_used([H|T], X) :-
   member(H, X),
   all_values_used(T, X).

% Kontrola celkového počtu prvků
check_list_counts(L, LGT, LLT) :-
   length_of_list(L, VL),
   length_of_list(LGT, VLGT),
   length_of_list(LLT, VLLT),
   VL is +(VLGT, VLLT).

% Kontrola čísti kde mají být vetší hodnoty
check_list_greater(LGT, L) :-
   sum_of_list_elem(L, Sum),
   length_of_list(L, Length),
   Avg is Sum / Length,
   check_list_items_greater(LGT, L, Avg).

% Porovnání jednotlivých prvků se střední hodnotou
check_list_items_greater([], _Z, _Avg).
check_list_items_greater([H|Y], Z, Avg) :- 
   member(H,Z),
   check_item_value_greater(H, Avg),
   check_list_items_greater(Y, Z, Avg).

check_item_value_greater(X, Y) :-
   obj_value(X, V),
   V >= Y.

% Kontrola čísti kde mají být menší hodnoty
check_list_less(LLT, L) :-
   sum_of_list_elem(L, Sum),
   length_of_list(L, Length),
   Avg is Sum / Length,
   check_list_items_less(LLT, L, Avg).

% Porovnání jednotlivých prvků se střední hodnotou
check_list_items_less([], _Z, _Avg).
check_list_items_less([H|Y], Z, Avg) :- 
   member(H,Z),
   check_item_value_less(H, Avg),
   check_list_items_less(Y, Z, Avg).

check_item_value_less(X, Y) :-
   obj_value(X, V),
   V < Y.

% Spočítání délky seznamu
length_of_list([], 0).
length_of_list([_H|T], N) :- 
   length_of_list(T, N1),
   N is N1 + 1.

% Spočítání sumy prvků v seznamu
sum_of_list_elem([], 0).
sum_of_list_elem([H|T], N) :- 
   sum_of_list_elem(T, N1),
   obj_value(H, V),
   N is N1 + V.

obj_value(obj(_X, Y), V) :-
   V is Y.

% Kontrola jestli je prvek v seznamu
member(X,[X | _ ]).
member(X,[ _ |T]) :-
   member(X, T), !.
